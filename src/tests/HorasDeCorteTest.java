package tests;

import static org.junit.Assert.*;
import org.junit.Test;

import horasDeCorte.Buscador;
import horasDeCorte.Dia;
import horasDeCorte.Materia;

import org.junit.Before;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HorasDeCorteTest {
	Date hora10, hora12, hora13, hora14, hora15, hora16, hora17, hora18, hora20, hora22;
	List<Materia> materias = new ArrayList<>();
	Materia materia1, materia2, materia3, materia4, materia5, materia6;
	Map<Date, Integer> horasYcortes = new HashMap<>();

	@Before
	public void init() {
		materia1 = new Materia("nombre", Dia.VIERNES, new Date(8), new Date(12));
		materia2 = new Materia("nombre", Dia.VIERNES, new Date(10), new Date(12));
		materia3 = new Materia("nombre", Dia.VIERNES, new Date(13), new Date(17));
		materia4 = new Materia("nombre", Dia.VIERNES, new Date(14), new Date(18));
		materia5 = new Materia("nombre", Dia.VIERNES, new Date(14), new Date(16));
		materia6 = new Materia("nombre", Dia.VIERNES, new Date(18), new Date(22));

		materias.add(materia1);
		materias.add(materia2);
		materias.add(materia3);
		materias.add(materia4);
		materias.add(materia5);
		materias.add(materia6);

		hora10 = new Date(10);
		hora12 = new Date(12);
		hora13 = new Date(13);
		hora14 = new Date(14);
		hora15 = new Date(15);
		hora17 = new Date(17);
		hora18 = new Date(18);
		hora20 = new Date(20);
		hora22 = new Date(22);

		Buscador pruebaAulas = new Buscador(materias);
		horasYcortes = pruebaAulas.darHorasCorte();
	}

	@Test
	public void horasCorte() {
		assertEquals((int) horasYcortes.get(hora12), 0);
		assertEquals((int) horasYcortes.get(hora13), 0);
		assertEquals((int) horasYcortes.get(hora18), 0);
		assertEquals((int) horasYcortes.get(hora22), 0);
	}

	@Test
	public void cantidadMateriasQueCortan() {
		assertEquals((int) horasYcortes.get(hora10), 1);
		assertEquals((int) horasYcortes.get(hora15), 3);
		assertEquals((int) horasYcortes.get(hora14), 1);
	}

	@Test
	public void horasNoSonDeCorte() {
		assertFalse((int) horasYcortes.get(hora17) == 0);
		assertFalse((int) horasYcortes.get(hora20) == 0);
	}
}

