package horasDeCorte;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Buscador {
	private Map<Date, Integer> _cantidadMateriasCortePorHora;
	private List<Materia> _materias;

	public Buscador(List<Materia> materiasDadas) {
		_materias = materiasDadas;
		inicializarHoras();
		chequearHorasCorte();
	}

	private void chequearHorasCorte() {
		for (Date hora : _cantidadMateriasCortePorHora.keySet()) {
			for (Materia materia : _materias) {
				if (!esHoraDeCorte(materia, hora))
					incrementarCuentaMateriasQueCortan(hora);
				// Una materia "corta" una hora cuando empieza antes y termina después de dicha
				// hora.
			}
		}
	}

	public Map<Date, Integer> darHorasCorte() {
		return new HashMap<>(_cantidadMateriasCortePorHora);
	}

	private void inicializarHoras() {
		_cantidadMateriasCortePorHora = new HashMap<>();

		for (int i = 8; i <= 22; i++)
			_cantidadMateriasCortePorHora.put(new Date(i), 0);
		// En un principio todas las horas pueden ser de corte
		// por lo que se les asigna 0 (cero) a c/u.
	}

	private void incrementarCuentaMateriasQueCortan(Date hora) {
		_cantidadMateriasCortePorHora.put(hora, _cantidadMateriasCortePorHora.get(hora) + 1);
	}

	private boolean esHoraDeCorte(Materia materia, Date hora) {
		return !(materia.getHoraInicio().compareTo(hora) < 0 && materia.getHoraFin().compareTo(hora) > 0);
		// Una hora es de corte si y solo si ninguna materia empieza antes ni termina
		// después.
	}

}
