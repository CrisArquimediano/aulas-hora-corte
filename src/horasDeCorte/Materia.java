package horasDeCorte;

import java.util.Date;

public class Materia {
	private String _nombre;
	private Dia _dia;
	private Date _horaInicio;
	private Date _horaFin;

	public Materia(String nombre, Dia dia, Date horaInicio, Date horaFin) {
		_nombre = nombre;
		_dia = dia;
		_horaInicio = horaInicio;
		_horaFin = horaFin;
	}

	public String getNombre() {
		return _nombre;
	}

	public Dia getDia() {
		return _dia;
	}

	public Date getHoraInicio() {
		return _horaInicio;
	}

	public Date getHoraFin() {
		return _horaFin;
	}

	@Override
	public String toString() {
		return _nombre + ", " + _dia + ", " + _horaInicio + ", " + _horaFin + "\n";
	}
}
